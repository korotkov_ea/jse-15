package ru.korotkov.tm.constant;

public class TerminalConst {

    public static final String SPLIT = " ";
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PROJECT_UPDATE = "project-update";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TASK_UPDATE = "task-update";
    public static final String TASK_VIEW_BY_PROJECT = "task-view-by-project";
    public static final String TASK_ADD_TO_PROJECT = "task-add-to-project";
    public static final String TASK_REMOVE_FROM_PROJECT = "task-remove-from-project";

    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW = "user-view";
    public static final String USER_REMOVE = "user-remove";
    public static final String USER_UPDATE = "user-update";
    public static final String USER_EXIT = "user-exit";
    public static final String USER_HISTORY = "user-history";

    public static final String REGISTER = "register";
    public static final String AUTHENTICATION = "authentication";

    public static final String PROFILE_SHOW = "profile-show";
    public static final String PROFILE_UPDATE = "profile-update";
    public static final String PROFILE_CHANGE_PASSWORD = "profile-change-password";

    public static final String OPTION_INDEX = "index";
    public static final String OPTION_NAME = "name";
    public static final String OPTION_ID = "id";
    public static final String OPTION_LOGIN = "login";

}