package ru.korotkov.tm.service;

import ru.korotkov.tm.entity.User;
import ru.korotkov.tm.enumerated.Role;
import ru.korotkov.tm.exception.DuplicateLoginException;
import ru.korotkov.tm.repository.UserRepository;
import ru.korotkov.tm.util.HashUtil;

import java.util.List;
import java.util.ResourceBundle;

public class UserService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String password) throws DuplicateLoginException {
        if (login == null || login.isEmpty()) {
            return null;
        }
        if (password == null || password.isEmpty()) {
            return null;
        }
        User user = findByLogin(login);
        if (user != null) {
            throw new DuplicateLoginException(bundle.getString("dublicateLoginException"));
        }
        return userRepository.create(login, HashUtil.hashMD5(password));
    }

    public User create(final String login, final String password, final Role role) throws DuplicateLoginException {
        if (role == null) {
            return null;
        }
        User user = this.create(login, password);
        if (user != null) {
            user.setRole(role);
        }
        return user;
    }

    public void clear() {
        userRepository.clear();
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        return userRepository.findByLogin(login);
    }

    public User findById(final Long id) {
        if (id == null) {
            return null;
        }
        return userRepository.findById(id);
    }

    public User findByIndex(int index) {
        if (index < 0 || index >= userRepository.getSize()) {
            return null;
        }
        return userRepository.findByIndex(index);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        return userRepository.removeByLogin(login);
    }

    public User removeById(final Long id) {
        if (id == null) {
            return null;
        }
        return userRepository.removeById(id);
    }

    public User removeByIndex(int index) {
        if (index < 0 || index >= userRepository.getSize()) {
            return null;
        }
        return userRepository.removeByIndex(index);
    }

    public User updateById(
            final Long id, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        if (id == null) {
            return null;
        }
        if (!checkUpdateData(password, role, firstName, middleName, lastName)) {
            return null;
        }
        return userRepository.updateById(id, HashUtil.hashMD5(password), role, firstName, middleName, lastName);
    }

    public User updateByLogin(
            final String login, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        if (!checkUpdateData(password, role, firstName, middleName, lastName)) {
            return null;
        }
        return userRepository.updateByLogin(login, HashUtil.hashMD5(password), role, firstName, middleName, lastName);
    }

    public User updateByIndex(
            final int index, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        if (index < 0 || index >= userRepository.getSize()) {
            return null;
        }
        if (!checkUpdateData(password, role, firstName, middleName, lastName)) {
            return null;
        }
        return userRepository.updateByIndex(index, HashUtil.hashMD5(password), role, firstName, middleName, lastName);
    }

    private boolean checkUpdateData(
            final String password, final Role role, final String firstName,
            final String middleName, final String lastName) {
        if (password == null || password.isEmpty()) {
            return false;
        }
        if (role == null) {
            return false;
        }
        if (firstName == null || firstName.isEmpty()) {
            return false;
        }
        if (middleName == null || middleName.isEmpty()) {
            return false;
        }
        if (lastName == null || lastName.isEmpty()) {
            return false;
        }
        return true;
    }

    public User authentication(final String login, final String password) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        if (password == null || password.isEmpty()) {
            return null;
        }
        User user = userRepository.findByLogin(login);
        if (user == null) {
            return null;
        }
        if (user.getPassword().equals(HashUtil.hashMD5(password))) {
            return user;
        }
        return null;
    }

    public User changePassword(final String password, final Long userId) {
        if (password == null || password.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        User user = userRepository.findById(userId);
        if (user != null) {
            user.setPassword(HashUtil.hashMD5(password));
        }
        return user;
    }

    public User updateProfile(
            final Long userId, final String firstName,
            final String middleName, final String lastName
    ) {
        if (userId == null) {
            return null;
        }
        if (firstName == null || firstName.isEmpty()) {
            return null;
        }
        if (middleName == null || middleName.isEmpty()) {
            return null;
        }
        if (lastName == null || lastName.isEmpty()) {
            return null;
        }
        User user = userRepository.findById(userId);
        if (user != null) {
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
        }
        return user;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
