package ru.korotkov.tm.service;

import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.exception.NotExistElementException;
import ru.korotkov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class ProjectService {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.create(name, userId);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Project project = create(name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    public Project findByIndex(final int index, final Long userId) {
        if (index < 0 || index >= projectRepository.getSize(userId)) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.findByIndex(index, userId);
    }

    public Project findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.findByName(name, userId);
    }

    public Project findById(final Long id, final Long userId) {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        return projectRepository.findById(id, userId);
    }

    public Project removeByIndex(final int index, final Long userId) throws NotExistElementException {
        if (index < 0 || index >= projectRepository.getSize(userId)) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Project project = projectRepository.removeByIndex(index, userId);
        if (project == null) {
            throw new NotExistElementException(bundle.getString("projectNotFoundByIndex"));
        }
        return project;
    }

    public Project removeByName(final String name, final Long userId) throws NotExistElementException {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Project project = projectRepository.removeByName(name, userId);
        if (project == null) {
            throw new NotExistElementException(bundle.getString("projectNotFoundByName"));
        }
        return project;
    }

    public Project removeById(final Long id, final Long userId) throws NotExistElementException {
        if (id == null) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Project project = projectRepository.removeById(id, userId);
        if (project == null) {
            throw new NotExistElementException(bundle.getString("projectNotFoundById"));
        }
        return project;
    }

    public Project updateByIndex(final int index, final String name, final Long userId) throws NotExistElementException {
        if (index < 0 || index >= projectRepository.getSize(userId)) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Project project = projectRepository.updateByIndex(index, name, userId);
        if (project == null) {
            throw new NotExistElementException(bundle.getString("projectNotFoundByIndex"));
        }
        return project;
    }

    public Project updateByIndex(final int index, final String name, final String description, final Long userId) throws NotExistElementException {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Project project = updateByIndex(index, name, userId);
        project.setDescription(description);
        return project;
    }

    public Project updateById(final Long id, final String name, final Long userId) throws NotExistElementException {
        if (id == null) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (userId == null) {
            return null;
        }
        Project project = projectRepository.updateById(id, name, userId);
        if (project == null) {
            throw new NotExistElementException(bundle.getString("projectNotFoundById"));
        }
        return project;
    }

    public Project updateById(final Long id, final String name, final String description, final Long userId) throws NotExistElementException {
        if (description == null || description.isEmpty()) {
            return null;
        }
        Project project = updateById(id, name, userId);
        project.setDescription(description);
        return project;
    }

    public List<Project> findAll(final Long userId) {
        if (userId == null) {
            return new ArrayList<>();
        }
        return projectRepository.findAll(userId);
    }

    public List<Project> findAll(final Long userId, Comparator<Project> comporator) {
        if (comporator == null) {
            return new ArrayList<>();
        }
        List<Project> projects = projectRepository.findAll(userId);
        if (projects == null) {
            return null;
        }
        projects.sort(comporator);
        return projects;
    }

}
