package ru.korotkov.tm.service;

import java.util.LinkedList;
import java.util.List;

public class SessionService {

    private Long userId;

    private LinkedList<String> commands = new LinkedList<>();

    public static final int HISTORY_SIZE = 10;

    /**
     * Get userId
     *
     * @return userId - id of user
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * Start new session
     *
     * @param userId - id of user
     */
    public void startSession(final Long userId) {
        if (userId == null) {
            return;
        }
        this.userId = userId;
        commands = new LinkedList<>();
    }

    /**
     * Close session
     */
    public void closeSession() {
        this.userId = null;
        commands = new LinkedList<>();
    }

    /**
     * Add command to history
     *
     * @param command - command
     */
    public void addCommand(final String command) {
        if (command == null || command.isEmpty()) {
            return;
        }
        commands.add(command);
        while (commands.size() > HISTORY_SIZE) {
            commands.poll();
        }
    }

    /**
     * Return history of command
     *
     * @return list of commands
     */
    public List<String> findAllCommand() {
        if (userId == null) {
            return new LinkedList<>();
        }
        return new LinkedList<>(commands);
    }

}