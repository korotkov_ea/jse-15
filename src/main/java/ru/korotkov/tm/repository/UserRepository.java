package ru.korotkov.tm.repository;

import ru.korotkov.tm.entity.User;
import ru.korotkov.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private final List<User> users = new ArrayList<>();

    /**
     * Create user by login and password
     *
     * @param login - login of user
     * @param password - password of user
     * @return created project
     */
    public User create(final String login, final String password) {
        final User user = new User(login, password);
        users.add(user);
        return user;
    }

    /**
     * Clear users
     */
    public void clear() {
        users.clear();
    }

    /**
     * Get size of repository
     */
    public int getSize() {
        return users.size();
    }

    /**
     * Find user by login
     *
     * @param login login of user
     * @return user or null
     */
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        return null;
    }

    /**
     * Find user by id
     *
     * @param id id of user
     * @return user or null
     */
    public User findById(final Long id) {
        for (final User user : users) {
            if (id.equals(user.getId())) {
                return user;
            }
        }
        return null;
    }

    /**
     * Find user by index
     *
     * @param index index of user
     * @return user or null
     */
    public User findByIndex(final int index) {
        return users.get(index);
    }

    /**
     * Remove user by login
     *
     * @param login login of user
     * @return user or null
     */
    public User removeByLogin(final String login) {
        User user = findByLogin(login);
        if (user != null) {
            users.remove(user);
        }
        return user;
    }

    /**
     * Remove user by id
     *
     * @param id id of user
     * @return user or null
     */
    public User removeById(final Long id) {
        User user = findById(id);
        if (user != null) {
            users.remove(user);
        }
        return user;
    }

    /**
     * Remove user by index
     *
     * @param index index of user
     * @return user or null
     */
    public User removeByIndex(final int index) {
        User user = findByIndex(index);
        if (user != null) {
            users.remove(user);
        }
        return user;
    }

    /**
     * Update user by id
     *
     * @param id
     * @param password
     * @param role
     * @param firstName
     * @param middleName
     * @param lastName
     * @return user or null
     */
    public User updateById(
            final Long id, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        User user = findById(id);
        if (user == null) {
            return null;
        }
        update(user, password, role, firstName, lastName, middleName);
        return user;
    }

    /**
     * Update project by login
     *
     * @param login
     * @param password
     * @param role
     * @param firstName
     * @param middleName
     * @param lastName
     * @return user or null
     */
    public User updateByLogin(
            final String login, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        User user = findByLogin(login);
        if (user == null) {
            return null;
        }
        update(user, password, role, firstName, lastName, middleName);
        return user;
    }

    /**
     * Update project by index
     *
     * @param index
     * @param password
     * @param role
     * @param firstName
     * @param middleName
     * @param lastName
     * @return user or null
     */
    public User updateByIndex(
            final int index, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        User user = findByIndex(index);
        if (user == null) {
            return null;
        }
        update(user, password, role, firstName, lastName, middleName);
        return user;
    }

    private void update(
            final User user, final String password, final Role role,
            final String firstName, final String middleName, final String lastName
    ) {
        user.setPassword(password);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
    }

    /**
     * Return projects
     */
    public List<User> findAll() {
        return new ArrayList<>(users);
    }

}