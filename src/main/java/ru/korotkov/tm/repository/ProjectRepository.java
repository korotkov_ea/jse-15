package ru.korotkov.tm.repository;

import ru.korotkov.tm.entity.Project;

import java.util.*;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    private final HashMap<String, HashSet<Project>> mapProjects = new HashMap<>();

    /**
     * Create project by name
     *
     * @param name - name of project
     * @param userId
     * @return created project
     */
    public Project create(final String name, final Long userId) {
        final Project project = new Project(name, userId);
        projects.add(project);
        addHashMap(project);
        return project;
    }

    /**
     * Create project by name and description
     *
     * @param name        - name of project
     * @param description - description of project
     * @param userId
     * @return created project
     */
    public Project create(final String name, final String description, final Long userId) {
        final Project project = create(name, userId);
        project.setDescription(description);
        return project;
    }

    /**
     * Add project to hashmap
     *
     * @param project - project
     */
    private void addHashMap(final Project project) {
        final String name = project.getName();
        HashSet<Project> projects = mapProjects.get(name);
        if (projects == null) {
            projects = new HashSet<>();
            projects.add(project);
            mapProjects.put(name, projects);
        } else {
            projects.add(project);
        }
    }

    /**
     * Remove project from hashmap
     *
     * @param project - project
     */
    private void removeFromHashMap(final Project project) {
        HashSet<Project> projects = mapProjects.get(project.getName());
        if (projects != null) {
            projects.remove(project);
        }
    }

    /**
     * Clear projects
     *
     * @param userId
     */
    public void clear(final Long userId) {
        final List<Project> removeProjects = findAll(userId);
        projects.removeAll(removeProjects);
        for (final Project project : removeProjects) {
            removeFromHashMap(project);
        }
    }

    /**
     * Get size of repository
     *
     * @param userId
     */
    public int getSize(final Long userId) {
        return findAll(userId).size();
    }

    /**
     * Find project by index
     *
     * @param index index of project
     * @param userId
     * @return project or null
     */
    public Project findByIndex(final int index, final Long userId) {
        Project project = findAll(userId).get(index);
        if (project == null) {
            return null;
        }
        if (!project.getUserId().equals(userId)) {
            return null;
        }
        return project;
    }

    /**
     * Find project by name
     *
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project findByName(final String name, final Long userId) {
        HashSet<Project> projects = mapProjects.get(name);
        if (projects == null) {
            return null;
        }
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) {
                return project;
            }
        }
        return null;
    }

    /**
     * Find project by id
     *
     * @param id id of project
     * @param userId
     * @return project or null
     */
    public Project findById(final Long id, final Long userId) {
        Project result = null;
        for (final Project project : projects) {
            if (id.equals(project.getId())) {
                result = project;
                break;
            }
        }
        if (result != null && !result.getUserId().equals(userId)) {
            return null;
        }
        return result;
    }

    /**
     * Remove project from structures
     *
     * @param project - project
     */
    private void remove(final Project project) {
        final String name = project.getName();
        projects.remove(project);
        HashSet<Project> setProjects = mapProjects.get(name);
        if (setProjects != null) {
            setProjects.remove(project);
        }
    }

    /**
     * Remove project by index
     *
     * @param index index of project
     * @param userId
     * @return project or null
     */
    public Project removeByIndex(final int index, final Long userId) {
        Project project = findByIndex(index, userId);
        if (project != null) {
            remove(project);
        }
        return project;
    }

    /**
     * Remove project by name
     *
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project removeByName(final String name, final Long userId) {
        Project project = findByName(name, userId);
        if (project != null) {
            remove(project);
        }
        return project;
    }

    /**
     * Remove project by id
     *
     * @param id id of project
     * @param userId
     * @return project or null
     */
    public Project removeById(final Long id, final Long userId) {
        Project project = findById(id, userId);
        if (project != null) {
            remove(project);
        }
        return project;
    }

    /**
     * Change name of project
     *
     * @param project project
     * @param name new name
     */
    private void changeNameOfProject(final Project project, final String name) {
        removeFromHashMap(project);
        project.setName(name);
        addHashMap(project);
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project updateByIndex(final int index, final String name, final Long userId) {
        Project project = findByIndex(index, userId);
        if (project == null) {
            return null;
        }
        changeNameOfProject(project, name);
        return project;
    }

    /**
     * Update project by index
     *
     * @param index index of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Project updateByIndex(final int index, final String name, final String description, final Long userId) {
        Project project = updateByIndex(index, name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    /**
     * Update project by id
     *
     * @param id id of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Project updateById(final Long id, final String name, final Long userId) {
        Project project = findById(id, userId);
        if (project == null) {
            return null;
        }
        changeNameOfProject(project, name);
        return project;
    }

    /**
     * Update project by id
     *
     * @param id id of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Project updateById(final Long id, final String name, final String description, final Long userId) {
        Project project = updateById(id, name, userId);
        if (project != null) {
            project.setDescription(description);
        }
        return project;
    }

    /**
     * Return projects
     *
     * @param userId
     */
    public List<Project> findAll(final Long userId) {
        ArrayList<Project> result = new ArrayList<Project>();
        for (final Project project: projects) {
            if (project.getUserId().equals(userId)) {
                result.add(project);
            }
        }
        return result;
    }

}