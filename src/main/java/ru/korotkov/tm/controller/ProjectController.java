package ru.korotkov.tm.controller;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.exception.NotExistElementException;
import ru.korotkov.tm.service.ProjectService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Create project
     *
     * @param arguments - arguments of command
     * @param userId
     */
    public void createProject(final String[] arguments, final Long userId) {
        final String name = arguments.length > 0 ? arguments[0] : null;
        final String description = arguments.length > 1 ? arguments[1] : null;
        if (description == null) {
            projectService.create(name, userId);
        } else {
            projectService.create(name, description, userId);
        }
        System.out.println(bundle.getString("projectCreate"));
    }

    /**
     * Clear projects
     *
     * @param userId
     */
    public void clearProject(final Long userId) {
        projectService.clear(userId);
        System.out.println(bundle.getString("projectClear"));
    }

    /**
     * View project
     *
     * @param arguments - arguments of command
     * @param userId
     */
    public void viewProject(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectService.findByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                project = projectService.findByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                project = projectService.findById(Long.parseLong(param), userId);
                break;
        }
        displayProject(project);
    }

    /**
     * Display project
     *
     * @param project project
     */
    public void displayProject(Project project) {
        if (project == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    /**
     * Update project
     *
     * @param arguments - arguments of command
     * @param userId
     */
    public void updateProject(final String[] arguments, final Long userId) throws NotExistElementException {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String name = arguments.length > 2 ? arguments[2] : null;
        final String description = arguments.length > 3 ? arguments[3] : null;
        if (option == null || param == null || name == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                if (description == null) {
                    project = projectService.updateByIndex(Integer.parseInt(param) - 1, name, userId);
                } else {
                    project = projectService.updateByIndex(Integer.parseInt(param) - 1, name, description, userId);
                }
                break;
            case TerminalConst.OPTION_ID:
                if (description == null) {
                    project = projectService.updateById(Long.parseLong(param), name, userId);
                } else {
                    project = projectService.updateById(Long.parseLong(param), name, description, userId);
                }
                break;
        }
        displayProject(project);
    }

    /**
     * List projects
     *
     * @param arguments - arguments of command
     * @param userId
     */
    public void listProject(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : "";

        List<Project> projects = Collections.EMPTY_LIST;
        switch (option) {
            case TerminalConst.OPTION_NAME:
                projects = projectService.findAll(userId, Comparator.comparing(Project::getName));
                break;
            case TerminalConst.OPTION_ID:
                projects = projectService.findAll(userId, Comparator.comparing(Project::getId));
                break;
            default:
                projects = projectService.findAll(userId);
                break;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println("INDEX: " + index++ + " ID: " + project.getId() + " " + project.getName() + ": " + project.getDescription());
        }
    }

}
