package ru.korotkov.tm.exception;

public class NotExistElementException extends Exception {

    public NotExistElementException(final String message) {
        super(message);
    }

}
