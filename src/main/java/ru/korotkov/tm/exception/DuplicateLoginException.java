package ru.korotkov.tm.exception;

public class DuplicateLoginException extends Exception {

    public DuplicateLoginException(final String message) {
        super(message);
    }

}
