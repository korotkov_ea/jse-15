# 1. Project information

Task manager application

# 2. Software requirements

Java 11, OS Windows

# 3. Technological stack

Java, Maven

# 4. Installation

## 4.1 Build project

```cmd
mvn clean install
```

## 4.2 Run application

```cmd
java -jar task-manager-1.0.15.jar
```

# 5. Supported commands

```cmd
version - Display program version.
about - Display developer info.
help - Display list of terminal commands.
project-create [name] [description] - Create project.
project-clear - Clear projects.
project-list [name|id] - Display list of projects. Optional sorted by name or id.
project-view {index|name|id} param - Display project by index, name or id.
project-remove {index|name|id} param [task] - Remove project by index, name or id. Option task remove related tasks.
project-update {index|id} param name [description] - Update project by index or id.
task-create [name] [DESCRIPTION] - Create task.
task-clear - Clear tasks.
task-list [name|id] - Display list of tasks. Optional sorted by name or id.
task-view {index|name|id} param - Display task by index, name or id.
task-remove {index|name|id} param - Remove task by index, name or id.
task-update {index|id} param name [description] - Update task by index or id.
task-view-by-project projectId - View tasks by projectId.
task-add-to-project projectId taskId - Add task to project.
task-remove-from-project taskId - Remove task from project.
user-clear - Clear users.
user-list - Display list of users.
user-view {index|login|id} param - Display user by index, login or id.
user-remove {index|login|id} param - Remove user by index, login or id.
user-update {index|login|id} param password role firstName middleName lastName - Update user by index, login or id.
user-history - History of commands.
user-exit - Close session.
register login password - Register user.
authentication login password - Authentication user.
profile-show - Show current profile.
profile-change-password password - Change password.
profile-update firstname middlename lastname - Change profile.
exit - Exit. 
```

# 6. Contact information

Evgeniy Korotkov (korotkov_ea@nlmk.com)